from setuptools import setup, find_packages

setup(name='Collectl2plotly',
      version='1',
      author='TU Dresden',
      python_requires=">=3.6",
      packages=find_packages(),
      package_data={'': ['plotly-latest.min.js']},
      scripts=[],
      entry_points='''
      [console_scripts]
      Collectl2plotly=Collectl2plotly:main
      ''',
      install_requires=[
        'yattag',
        'click',
      ],
)
