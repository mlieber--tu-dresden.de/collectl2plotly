import re
import gzip
import subprocess
from pathlib import Path
import copy
import time
import datetime
import multiprocessing as mp
import shutil
import importlib
import pickle
import json

import click
from yattag import Doc


import Collectl2plotly.filter_func as filter_func
import Collectl2plotly.value_merger as value_merger
import Collectl2plotly.Collectl2plotly_Plots_Config as default_plot_conf


### Filter Configs

# registered filter functions
# filter functions must be in filter_func.py
# key  : Display name
# value: function name
FILTER_FUNCTIONS = {'hardvalue': 'filter_hardvalue', 'average': 'filter_average'}

# Default filter function if no explicit is given
# must be a display name of a function
DEFAULT_FILTER = 'average'

### validation Config

# Merger that exist and possible kwargs/parameter
# key   = merger_func name
# value = dict with kwarguments of merger_func 
# kwargument dict:
# key = argument name
# value = 1. 'type' with a type obj or a tuple of type objs 
#         or 
#         2. 'choices' list of possible choices for the kwargument
MERGER = {
    'x2oneaddition_int': {
        'roundingpoint': {'type':int},
        'operator': {'choices': ['+', '-', '*', '/', '%']},
        'operator_value': {'type': (int, float)},
    },
    'x2oneaddition_float': {
        'roundingpoint': {'type':int},
        'operator': {'type': str, 'choices': ['+', '-', '*', '/', '%']},
        'operator_value': {'type': (int, float)},
    },
}

# validation infos of config
# key = config variablename to validate
# value = validator dict or type/tuple of types
# hints!!!!
# '__merger__' can be used as type for the merger tuple.
# in fixed_keys and 'changing_keys' the 'validator' need not be given if sub-elements are not to be validated.
#
# validator dict has follow structur:
# 'type' : required, is the possible type/s of the obj that are valid
# 'emptyable': list/dict/tuple can be empty if emptyable is True. Default: False
# 'subvalue_validator': is a validatordict or type/tuple of types to check all elems are valid if type is list/tuple
# 'fixed_keys': only if type is dict. takes a dict:
#               key: a known key of the current dict that is checked
#               value: dict with following possible keys:
#                     'required': if False the key must not in the dict Default: True
#                     'validator':  is a validatordict or type/tuple of types to validate the value
# 'changing_keys': only if type is dict. takes a dict:
#               'key_type': type of the keys in the dict current dict
#               'validator':  is a validatordict or type/tuple of types to validate the values of the dict
#
VALIDATIONS_INFOS = {
    'NEEDED_VALUES_DEFAULTS': {
        'type': dict,
        'fixed_keys': {
            'default_base_value': {},
            'default_merger': {'validator': '__merger__'},
        },
    },
    'NEEDED_VALUES': {
        'type': dict,
        'changing_keys': {
            'key_type': str,
            'validator': {
                'type': dict,
                'fixed_keys': {
                    'keys': {'validator': {'type': list, 'subvalue_validator': str}},
                    'merger': {'validator': '__merger__'},
                    'base_value': {'required': False}
                },
            },
        },
    },
    'PLOTS_CONFIG': {
        'type': list,
        'subvalue_validator': {
            'type': dict,
            'fixed_keys': {
                'name': {'validator': str},
                'needed_key': {'validator': str},
                'relative-absolute_xaxis': {'required': False, 'validator': bool},
                'plotly_settings': {'validator': dict},
            },
        },
    },
    'NAME_SPEZIAL_PARAMETER': {
        'type': dict,
        'emptyable': True,
        'changing_keys': {
            'key_type': str,
            'validator': {
                'type': list,
                'emptyable':True,
                'subvalue_validator': str,
            },
        },
    },
    'COMAND_BLACKLIST': {
        'type': list,
        'emptyable': True,
        'subvalue_validator': str,
    },
    'PLOTLY_COLORS': {
        'type': list,
        'subvalue_validator': str,
    },
    'PLOTLY_STATIC_COLORS': {
        'type': dict,
        'emptyable': True,
        'changing_keys': {
            'key_type': str,
            'validator': str,
        },
    },
}


### Other

# config for find best axis interval and unit
# from smallest unit to greatest
# intervals = possible intervals of the axis(needed)
# max = conversion value if not given greatest supported unit
# unit = str with unit name
AXIS_INTERVAL_REL = [
    {
        'max': 60,
        'intervals': [1, 5, 10],
        'unit': 'sec',
    },
    {
        'max': 60,
        'intervals': [0.5, 1, 5, 10],
        'unit': 'min',
    },
    {
        'intervals': [0.25 ,0.5, 1, 5, 10],
        'unit': 'h',
    }
]

#pattern for split of commands (with \" and \ )
FIELDS_PATTERN = re.compile(r'(?:(?:\s*[^\\]|\A)\"(.*?[^\\]|)\"|(?:\s+|\A)(?=[^\s])(.*?[^\\])(?= |\Z))')
#(?:\"(.*?)\"|(\S+))



def is_merger(to_check):
    """Check is given obj a valid merger. Possible merger are given in MERGER.

    Arguments:
        to_check {unkown} -- obj to test

    Returns:
        (bool, str) -- return true if it is a merger false and error if not
    """

    # check is tuple with 2 elems
    try:
        name, mergerkwargs = to_check
    except ValueError:
        return False, "'{0}' has no right merger structur expected: (name[str], kwargs[dict])".format(to_check)

    #check name and mergerkwargs has right types
    if isinstance(name, str) and isinstance(mergerkwargs, dict):
        # check is the given merger known
        if name in MERGER:
            # check every kwarg of the given merger is it known and is right value for it given.
            for kwarg_name, value in mergerkwargs.items():
                if kwarg_name in MERGER[name]:
                    if 'choices' in MERGER[name][kwarg_name]:
                        if not value in MERGER[name][kwarg_name]['choices']:
                            return False, '{0} {1} expect one of {2} got: {3}'.format(
                                name, kwarg_name, MERGER[name][kwarg_name]['choices'], value
                            )
                    else:
                        if not isinstance(value, MERGER[name][kwarg_name]['type']):
                            return False, '{1} of {0} expect {2} not {3}'.format(
                                name, kwarg_name, MERGER[name][kwarg_name]['type'], type(value)
                            )
                else:
                    return False, '{1} is unknown parameter of {0}'.format(name, kwarg_name)
        else:
            return False, '"{0}" is unknown merger'.format(name)
    else:
        return False, '"{0}" has no right merger structur expected: (name[str], kwargs[dict])'.format(to_check)

    return True, None


def check_valid(to_check, validationinfos):
    """check object is valid from the perspective of validationinfos

    Arguments:
        to_check {unkown} -- object to check is valid
        validationinfos {validationdict(see comment of VALIDATIONS_INFOS) or type or tuple with types} -- infos what is needed that to_check is valid

    Returns:
        (bool, str) -- return true if it is valid, false and error if not
    """
    # check is validationinfos a validationdict or only a type/tuple of types
    if isinstance(validationinfos, dict):
        if validationinfos['type'] == '__merger__':
            return is_merger(to_check)
        else:
            # check is given elem expected type
            if isinstance(to_check, validationinfos['type']):
                # if elem dict check special cases of it
                if type(to_check) == dict:
                    # check is elem is empty and is this ok
                    if not validationinfos.get('emptyable', False) and not len(to_check):
                        return False, '{0} can not be empty!'.format(to_check)

                    # check special keys and there values if 'fixed_keys' is given
                    for fixed_name, infos in validationinfos.get('fixed_keys', {}).items():
                        if fixed_name in to_check:
                            if 'validator' in infos:
                                valid, error_msg = check_valid(to_check[fixed_name], infos['validator'])
                                if not valid:
                                    return valid, '[{0}] => {1}'.format(fixed_name, error_msg)

                        elif infos.get('required', True):
                            return False, '{0} missing key: "{1}"'.format(to_check, fixed_name)

                    # check all keys and there values if 'changing_keys' is given
                    if 'changing_keys' in validationinfos:
                        for key, subvalue in to_check.items():
                            if not isinstance(key, validationinfos['changing_keys']['key_type']):
                                return False, '{0} key has wrong type expected {1} got {2}'.format(
                                    key,  validationinfos['changing_keys']['key_type'], type(key),
                                )
                            if 'validator' in validationinfos['changing_keys']:
                                valid, error_msg =  check_valid(subvalue, validationinfos['changing_keys']['validator'])
                                if not valid:
                                    return valid, '[{0}] => {1}'.format(key, error_msg)
                # if elem list or tuple check special cases of it                 
                elif isinstance(to_check, (list, tuple)):
                    # check is elem is empty and is this ok
                    if not validationinfos.get('emptyable', False) and not len(to_check):
                        return False, '{0} can not be empty!'.format(to_check)

                    # validate all values of tuple/list if 'subvalue_validator' is given
                    if 'subvalue_validator' in validationinfos:
                        for i, subvalue in enumerate(to_check):
                            valid, error_msg = check_valid(subvalue, validationinfos['subvalue_validator'])
                            if not valid:
                                return valid, '[{0}] => {1}'.format(i, error_msg)

            else:
                return False, '{0} is wrong type expected {1} got {2}'.format(
                        to_check, validationinfos['type'], type(to_check)
                    )

    else:
        # check it is the special case that is the type is '__merger__'
        if validationinfos == '__merger__':
            return is_merger(to_check)
        else:
            # check is given elem expected type
            if not isinstance(to_check, validationinfos):
                return False, '{0} is {1} expected: {2}'.format(
                    to_check, type(to_check), validationinfos,
                )

    return True, None


def datestr2date(datestr):
    """Converts a "datestring" to a date Object.

    Arguments:
        datestr {str} -- string of a Date example: 20191224

    Returns:
        datetime.date -- date of the string
    """
    return datetime.date(
        int(datestr[:4]),
        int(datestr[4:6]),
        int(datestr[6:8]),
    )


def get_cmdname(cmd, spezial_parameters_of_all, coarsest=False):
    """search in complete commandstring the name of the skript or the command that is used

    Arguments:
        cmd {str} -- complete commandstring

    Keyword Arguments:
        coarsest {bool} -- return only the call function(example: bash, python) if True (default: {False})

    Returns:
        str -- new cmd name
    """
    # search for (for example) bash_function="python" in cmd="/usr/bin/python3 my_script.py"
    # split command with regex
    cmd_splited = [x[0] or x[1] for x in FIELDS_PATTERN.findall(cmd)]
    #shlex.split(cmd)
    bash_function = cmd_splited[0].split('/')[-1]
    bash_function = re.search(r'[^\W\n]+', bash_function).group(0)
    # check if bash_function is known, if not, return bash_function
    spezial_parameters = spezial_parameters_of_all.get(bash_function, None)
    if coarsest or spezial_parameters is None:
        return bash_function
    skip = False
    # search script/program name within the parameters and only return this without path
    for position, parameter in enumerate(cmd_splited[1:]):
        if skip:
            skip = False
            continue
        if parameter in spezial_parameters:
            skip = True
            continue
        if bash_function == 'bash' or bash_function == 'sh' and parameter == '-c':
            return bash_function + ' -c'
            # return cmd_splited[position+1]
        if parameter.startswith('-'):
            continue
        return parameter.split('/')[-1]
    return cmd


def parse_file(path, collectl, shorten_cmds, coarsest, config):
    """start subproccess collectl than parse the output and merge.
       After that build usefull dict from parsed data

    Arguments:
        path {Path} -- path to collectl file to parse
        collectl {str} -- collectl command
        shorten_cmds {bool} -- if True cmd will be shorted by get_cmdname
        coarsest {bool} -- parameter of get_cmdname
        config {dict} -- plots and merge Config


    Returns:
        (dict, dict) --  1. plot_data = {comands : {metrics: [values, ...],  ...}, ...}
                         2. data for filter function :
                         {'number_of_values': X, 'commands':{ cmd:{'number_of_values': X, metrics:SUM, ...}}, metrics:SUM, ...}
    """
    collectl_starttime = time.time()
    # run collectl in playback mode and read output into list
    print('start collectl replay')
    process = subprocess.run(
        [collectl, '-P', '-p', path, '-sZ'], stdout=subprocess.PIPE, check=True,
    )
    print('collectl replay took {:.1f}s'.format(
        time.time() - collectl_starttime))
    parsing_starttime = time.time()
    # output contains all data!
    output = process.stdout.decode().splitlines()
    # get table head
    head = output.pop(0).split(' ')
    for possible_head in output[:]:
        if possible_head.startswith('#'):
            head = possible_head.split(' ')
            output.remove(possible_head)
        else:
            break

    # get template of an entry from the head
    head[0] = head[0][1:]
    head_indexes_dict = {
        head_title: index for index, head_title in enumerate(head)}
    empty_dict_with_value_titles = {
        value_title: copy.deepcopy(
            value_title_settings.get(
                'base_value', config['NEEDED_VALUES_DEFAULTS']['default_base_value']
            )
        ) for value_title, value_title_settings in config['NEEDED_VALUES'].items()
    }

    # parse all output lines
    entrys_data = {}
    cmd_cmdshort_dict = {}
    merger_lookup_dict = {}

    for entry in output:
        # split by ' ' (exclude command from splitting)
        splited_entry = entry.split(' ', len(head_indexes_dict)-1)
        # get command string and shorten
        cmd = splited_entry[-1]
        if shorten_cmds or coarsest:
            if cmd in cmd_cmdshort_dict:
                cmd = cmd_cmdshort_dict[cmd]
            else:
                short_cmd = get_cmdname(cmd, config['NAME_SPEZIAL_PARAMETER'], coarsest=coarsest)
                cmd_cmdshort_dict[cmd]= short_cmd
                cmd = short_cmd
        if cmd in config['COMAND_BLACKLIST']:
            continue
        # create dict for each command
        if not cmd in entrys_data:
            entrys_data[cmd] = {}
        # get datetime obj for current entry
        tmp_datetime = datetime.datetime.combine(
            datestr2date(splited_entry[head_indexes_dict['Date']]),
            datetime.time(*[int(n) for n in splited_entry[head_indexes_dict['Time']].split(':')]),
        )
        # if datetime not yet existing, add new entry from template
        if not tmp_datetime in entrys_data[cmd]:
            entrys_data[cmd][tmp_datetime] = pickle.loads(pickle.dumps(
                empty_dict_with_value_titles, -1))
        # get values from data as given in NEEDED_VALUES and run specified merger function
        # to merge multiple values with same timestamp or rescale (e.g. to GB)
        for value_title, value_title_settings in config['NEEDED_VALUES'].items():
            merger, merger_kwargs = value_title_settings.get('merger', config['NEEDED_VALUES_DEFAULTS']['default_merger'])
            if not merger in merger_lookup_dict:
                merger_lookup_dict[merger] = getattr(value_merger, merger)
            entrys_data[cmd][tmp_datetime][value_title] = merger_lookup_dict[merger](
                entrys_data[cmd][tmp_datetime][value_title],
                *[splited_entry[head_indexes_dict[key]] for key in value_title_settings['keys']],
                **merger_kwargs
            )
    print('parsing/merge took {:.1f}s'.format(time.time() - parsing_starttime))
    dictbuild_starttime = time.time()

    # create lists entry_data_plotfriendly[cmd][metric] = [ values, ... ] with the actual data to plot
    # and sum up all metrics for each command to enable filtering of non-interesting commands later on
    entry_data_plotfriendly = {}
    plot_filter_data = pickle.loads(pickle.dumps(empty_dict_with_value_titles, -1))
    plot_filter_data['number_of_values'] = 0
    plot_filter_data['commands'] = {}
    for cmd, cmd_data in entrys_data.items():
        plot_filter_data['commands'][cmd] = pickle.loads(pickle.dumps(
            empty_dict_with_value_titles, -1))
        plot_filter_data['commands'][cmd]['number_of_values'] = 0
        entry_data_plotfriendly[cmd] = {key: [] for key in config['NEEDED_VALUES']}
        entry_data_plotfriendly[cmd]['datetime'] = []
        for cmd_data_time, cmd_data_values in cmd_data.items():
            entry_data_plotfriendly[cmd]['datetime'].append(cmd_data_time)
            for cmd_data_key, cmd_data_value in cmd_data_values.items():
                entry_data_plotfriendly[cmd][cmd_data_key].append(cmd_data_value)

                plot_filter_data['commands'][cmd][cmd_data_key] += cmd_data_value
                plot_filter_data['commands'][cmd]['number_of_values'] += 1
                plot_filter_data[cmd_data_key] += cmd_data_value
                plot_filter_data['number_of_values'] += 1
    print(
        'data dict/ filter_data_dict build took {:.1f}s'.format(
            time.time() - dictbuild_starttime),
    )
    return entry_data_plotfriendly, plot_filter_data


def make_relative_xaxi(all_values):
    """build lists with relative values for xaxis

    Arguments:
        all_values {[datetime, ..]} -- list of all datetime objects for plot

    Returns:
        (list, list) -- 1.: list with the relative values that are displayed on the xaxis
                        2.: list with the datetime as str of the absolute values of xaxis
    """
    min_value = min(all_values)
    max_value = max(all_values)

    # Is the reference value where the next possible value is searched. For a good number of ticks on the axis.
    guideline_tickcounts = 10

    end_value = (max_value - min_value).total_seconds()
    conversion_factor = 1

    for current_unit in AXIS_INTERVAL_REL:
        #  Convert to higher units until the correct one is sufficient
        if 'max' in current_unit and end_value > current_unit['max']:
            end_value /= current_unit['max']
            conversion_factor *= current_unit['max']
            continue

        # find out the best possible interval
        distances = [
            max(end_value // interval - guideline_tickcounts, guideline_tickcounts - end_value // interval) \
                for interval in current_unit['intervals']
        ]
        interval = current_unit['intervals'][distances.index(min(distances))]

        # make array with plotly axis data
        xaxis_ticks = [interval * i for i in range(int(end_value/interval)+1)]
        if len(xaxis_ticks) == 1:
            xaxis_ticks.append(1)
        xaxis_ticks_values = [str(min_value + datetime.timedelta(seconds=tick * conversion_factor)) for tick in xaxis_ticks]
        return xaxis_ticks, xaxis_ticks_values, current_unit['unit']


def make_plot(cmds_data, filter_info, cmd_color, plot_settings, plotly_format_vars, needed_key, relative_xaxis):
    """Build a list of dicts in Plotlyconf style for the diffrent traces with the data from cmds_data of needed_key
        than make plot dict in Plotly style and change '{host}' in titels.

    Arguments:
        cmds_data {dict} -- is the data for the plot example: {'Command': {'datetime':[...], 'cpu': [...]}}
        filter_info {dict} -- has the information which cmds will be shown example: {'cpu': [cmds]}
        cmd_color {dict} -- assign a fixed plot color to each cmd example:  {'Command': 'rgb(0, 0, 128)'}
        plot_settings {dict} -- settings for Plotly
        plotly_format_vars {dict} -- values for Plotly settings
        needed_key {str} -- key of cmds_data for the values to be use
        relative_xaxis {bool} -- if true add buttons to change xaxis to relative

    Returns:
        dict -- with plotly jsons
    """
    plot_data = []
    # make plotly plots config
    for cmd in filter_info[needed_key]:
        plot_data.append({
            'x': [str(date) for date in cmds_data[cmd]['datetime']],
            'y': cmds_data[cmd][needed_key],
            'name': cmd,
            'marker': {
                'color': cmd_color[cmd],
            },
            **plot_settings['data'],
        }
        )


    layout = pickle.loads(pickle.dumps(plot_settings.get('layout', {}), -1))

    # replace {host} in titles
    if 'title' in layout:
        if type(layout['title']) == str:
            layout['title'] = layout['title'].format(
                **plotly_format_vars,
            )
        else:
            layout['title']['text'] = layout['title']['text'].format(
                **plotly_format_vars,
            )

    if 'yaxis' in layout and 'title' in layout['yaxis']:
        if type(layout['yaxis']['title']) == str:
            layout['yaxis']['title'] = layout['yaxis']['title'].format(
                **plotly_format_vars,
            )
        else:
            layout['yaxis']['title']['text'] = layout['yaxis']['title']['text'].format(
                **plotly_format_vars,
            )
    xtitle = ''
    if 'xaxis' in layout and 'title' in layout['xaxis']:
        if type(layout['xaxis']['title']) == str:
            layout['xaxis']['title'] = layout['xaxis']['title'].format(
                **plotly_format_vars,
            )
            xtitle = layout['xaxis']['title']
        else:
            layout['xaxis']['title']['text'] = layout['xaxis']['title']['text'].format(
                **plotly_format_vars,
            )
            xtitle = layout['xaxis']['title']['text']

    # add buttons for switching xaxis if 'relative-absolute_xaxis' in PLOT_CONF is set.
    if relative_xaxis:
            ticks_name, ticks_value, unit = make_relative_xaxi(
                [date for cmd in filter_info[needed_key] for date in cmds_data[cmd]['datetime']],
            )
            if not 'xaxis' in layout:
                layout['xaxis'] = {}
            if not 'updatemenus' in layout:
                layout['updatemenus'] = []
            layout['updatemenus'].append({
                'type': 'buttons',
                'x': 0.6,
                'y': 1.15,
                'direction': 'left',
                'buttons': [
                    {'args':[{'xaxis':{'title': xtitle, 'tickvals': None}}],
                    'label':'absolute xaxis',
                    'method':'relayout'},
                    {'args':[{'xaxis': {'title': 'runtime in {}'.format(unit), 'tickvals': ticks_value, 'ticktext': ticks_name}}],
                    'label':'relative xaxis',
                    'method':'relayout'},
                ],
                'showactive':True,
            })

    return {'data': json.dumps(plot_data), 'layout': json.dumps(layout), 'config': json.dumps(plot_settings.get('config', {}))}


def build_html(plots_dict):
    """build html site with the plots in plotsdict

    Arguments:
        plots_dict {dict} -- key: name of plot value: [plot, ...]

    Returns:
        str -- html website
    """
    doc, tag, text = Doc().tagtext()
    doc.asis('<!DOCTYPE html>')
    with tag('html'):
        with tag('head'):
            with tag('script', src='plotly.js'):
                pass
            # build plotly javascipt in html
            with tag('script'):
                doc.asis("document.onreadystatechange = () => {if (document.readyState === 'complete') {")
                for name, plots in plots_dict.items():
                    for i, plot in enumerate(plots):
                        doc.asis("Plotly.newPlot(document.getElementById('{name}-plot-{number}'), JSON.parse('{data}'), JSON.parse('{layout}'), JSON.parse('{config}'));"\
                            .format(name=name, number=i, data=plot['data'], layout=plot['layout'], config=plot['config'])
                        )
                doc.asis('}}')
        with tag('body'):
            with tag('div', id='index'):
                with tag('h2'):
                    text('Index:')
                with tag('ul'):
                    for name in plots_dict:
                        with tag('li'):
                            with tag('a', href='#'+name):
                                text(name)
            # add div for all plots in html
            for name, plots in plots_dict.items():
                with tag('div', id=name):
                    for i in range(len(plots)):
                        with tag('div', id='{}-plot-{}'.format(name, i)):
                            pass

    return doc.getvalue()


def get_sources(sources):
    """collect all sources (.raw.gz files)

    Arguments:
        sources {tuple} -- tuple of files/directorys

    Returns:
        list -- list of all .raw.gz files which was found
    """
    source_paths = []
    for source in sources:
        source_path =  Path(source)
        if source_path.is_dir():
            source_paths.extend(source_path.glob('*.raw.gz'))
        elif source_path.is_file() and source.endswith('.raw.gz'):
            source_paths.append(source_path)
    return source_paths


def data_from_file(arguments):
    """make data and filter infos of one collectl file

    Arguments:
        arguments {tuple} -- is a tuple of arguments for the function:
                    path {Path} -- collectldata file path
                    collectl {str} -- collectl command
                    shorten_cmds {bool} -- para for parse_file
                    coarsest {bool} -- para for parse_file
                    filtercmds {bool} -- if True cmds will be filter else not
                    filtervalue {int} -- para for filter
                    filtertype {str} -- which filter will be called
                    config {dict} -- plots and merge Config
    Returns:
        (str, dict, dict) -- 1. hostname
                             2. plot_data = {comands : {metrics: [values, ...],  ...}, ...}
                             3. filter_infos = {metrics: [cmds, ...]
    """
    path, collectl, shorten_cmds, coarsest, filtercmds, filtervalue, filtertype, config = arguments
    host = ''
    # get hostname
    with gzip.open(path, 'r') as f:
        for line in f:
            if line.startswith(b'# Host:'):
                host = re.search(r'# Host: *([^ ]+)', line.decode()).group(1)
    
    data, filter_data = parse_file(path, collectl, shorten_cmds, coarsest, config)

    filter_infos = None
    if filtercmds:
        filter_infos = getattr(filter_func, filtertype)(filter_data, filtervalue)


    # make new dict with sorted cmd from big to small
    filter_infos_sorted = {}
    for metric in config['NEEDED_VALUES']:
        filter_infos_sorted[metric] = []
        # go over all sorted commands and add if is not filtered out
        for (cmd, _) in sorted(filter_data['commands'].items(), key=lambda x: x[1][metric], reverse=True):
            if not filtercmds or cmd in filter_infos[metric]:
                filter_infos_sorted[metric].append(cmd)

    return host, data, filter_infos_sorted


@click.command(help='Generate htmlfiles with Plotlyplots with data from collectlfiles(".raw.gz")')
@click.option('--source', '-s', multiple=True, default=['.'], show_default=True, type=click.Path(exists=True), help='source for the plots. (collectl data(.raw.gz) file or directory with collectl data) multiple useable')
@click.option('--collectl', '-c', default='collectl', show_default=True, help='collectl command')
@click.option('--plotlypath', '-p', type=click.Path(exists=True), help='path to plotly.js')
@click.option('--destination', '-d', default='.', type=click.Path(exists=True), show_default=True, help='path to directory where directory with plots will be created')
@click.option('--configpath', default=None, type=click.Path(exists=True), help='python file with plot and merge infos see doku for detail')
@click.option('--shorten/--noshorten', default=True, help='commands will be shorted only to name')
@click.option('--coarsest', is_flag=True, help='commands will be shorted only to type (bash, perl, ...)')
@click.option('--filtercmds/--nofiltercmds', default=True, help='filtering or not')
@click.option('--filtervalue', help='Parameter which is given to the filter.')
@click.option('--filtertype',
              type=click.Choice(FILTER_FUNCTIONS.keys(), case_sensitive=False),
              default=DEFAULT_FILTER, show_default=True,
              help='Filter which is to be used.',
              )
@click.option('--force', '-f', is_flag=True, help='override existing plot directory if exist')
@click.option('--showcommands', is_flag=True, help='print all commands that are in the Plot')
def main(source, collectl, plotlypath, destination, configpath,
         shorten, coarsest, filtercmds, filtervalue, filtertype, force, showcommands):
    source_paths = get_sources(source)

    if not source_paths:
        print('no valid source found')
        exit(1)

    if not Path(destination).is_dir():
        print('destination is no valid directory')
        exit(1)

    plots_dir = Path(destination, 'collectlplots')
    if plots_dir.is_dir():
        if force:
            shutil.rmtree(plots_dir)
        else:
            print('in destination "collectlplots" already exist')
            exit(1)

    # load custome conf if given
    config_module = default_plot_conf
    if configpath:
        if configpath.endswith('.py'):
            config_module = importlib.machinery.SourceFileLoader('config', configpath).load_module()
        else:
            print('given config isn`t a ".py" Python file')
            exit(1)

    # validate config and make dict from values
    config = {}
    for config_var_name, validator_dict in VALIDATIONS_INFOS.items():
        try:
            config[config_var_name] = getattr(config_module, config_var_name)
        except AttributeError:
            print('{} missing in config'.format(config_var_name))
            exit(1)
        valid, errormsg = check_valid(config[config_var_name], validator_dict)
        if not valid:
            print('In {0}: \n{1}'.format(config_var_name, errormsg))
            exit(1)

    data_colllect_functions = []
    for source_path in source_paths:
        data_colllect_functions.append((source_path,
                                        collectl,
                                        shorten,
                                        coarsest,
                                        filtercmds,
                                        filtervalue,
                                        FILTER_FUNCTIONS[filtertype],
                                        config,
                                        ))

    # use multiprocessing to parse all collectl output files independently from each other in parallel
    pool = mp.Pool(min(len(data_colllect_functions), mp.cpu_count()))
    results = pool.map(data_from_file, data_colllect_functions)
    pool.close()
    hosts_data = {}
    cmd_all = set()

    for host, data, filter_infos in results:
        hosts_data[host] = {'data': data, 'filter_infos': filter_infos}
        cmd_all.update([cmd for cmds in filter_infos.values() for cmd in cmds])

    cmd_colors = {cmd: config['PLOTLY_COLORS'][i % len(config['PLOTLY_COLORS'])] for i, cmd in enumerate(sorted(list(cmd_all)))}
    cmd_colors.update(config['PLOTLY_STATIC_COLORS'])

    # for each host and each plot (as given in config) call make_plot to create html div with plotly
    start_plots_build = time.time()
    plots_dict = {}
    for host, host_data in hosts_data.items():
        plots_dict[host] = {plot_config['name']: [] for plot_config in config['PLOTS_CONFIG']}
        for plot_config in config['PLOTS_CONFIG']:
            plots_dict[host][plot_config['name']].append( make_plot(
                host_data['data'],
                host_data['filter_infos'],
                cmd_colors,
                plot_config['plotly_settings'],
                {'host': host},
                plot_config['needed_key'],
                plot_config.get('relative-absolute_xaxis', False),
            ))
    print("plots build in {:.1f}s".format(time.time() - start_plots_build))


    # create output directory and copy plotly.js
    plots_dir.mkdir()
    if plotlypath is None:
        plotlypath = Path(__file__).with_name('plotly-latest.min.js')
    try:
        shutil.copy(plotlypath, str(Path(plots_dir, 'plotly.js')))
    except shutil.SameFileError:
        pass

    # for each host create html file with plots
    time_sites = time.time()
    for host, site_plots in plots_dict.items():
        with Path(plots_dir, host+'-plots.html').open(mode='w') as plots_file:
            plots_file.write(build_html(site_plots))
    print("write/build of websites took {:.1f}s".format(time.time() - time_sites))


    if showcommands:
        print('commands:')
        for command in cmd_colors.keys():
            print(command)


if __name__ == '__main__':
    main()
