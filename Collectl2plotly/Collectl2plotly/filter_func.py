

def filter_hardvalue(filter_data, filtervalue):
    """sort commands from greatest to smallest value in relation to the overall value (percentage).
    Than add all commands to filter_info until filtervalue is smaller as all percentages of the commands.

    Arguments:
        filter_data {dict} -- dict with all data needed for filter func
        filtervalue {str} -- percentage of the interesting values

    Returns:
        dict -- dict key= collectl heads value=list of cmd that have not been filtered out
    """
    if filtervalue is None:
        filtervalue = 95
    else:
        filtervalue = int(filtervalue)

    filter_info = {}
    tmp_sort_data = {}

    for cmd, cmd_data in filter_data['commands'].items():
        for cmd_data_key, cmd_data_value in cmd_data.items():
            if cmd_data_key == 'number_of_values':
                continue
            if cmd_data_key not in tmp_sort_data:
                tmp_sort_data[cmd_data_key] = []
            tmp_sort_data[cmd_data_key].append((
                int(100 * (cmd_data_value) / filter_data[cmd_data_key]),
                cmd,
            ))

    for key, values in tmp_sort_data.items():
        current_percent = 0
        for (percent, cmd) in sorted(values, key=lambda x: x[0], reverse=True):
            if percent == 0 or current_percent >= filtervalue:
                break
            current_percent += percent

            if key not in filter_info:
                filter_info[key] = []

            filter_info[key].append(cmd)

    return filter_info


def filter_average(filter_data, filtervalue):
    """filters out the commands which are below the filtervalue(percentage)
    in relation to the overall average.

    Arguments:
        filter_data {dict} -- dict with all data needed for filter func
        filtervalue {int} -- percentage of min needed avarage in relation to the overall average

    Returns:
        dict -- dict key= collectl heads value=list of cmd that have not been filtered out
    """
    if filtervalue is None:
        filtervalue = 5
    else:
        filtervalue = int(filtervalue)

    all_averages = {}
    filter_info = {}
    for cmd, cmd_data in filter_data['commands'].items():
        for cmd_data_key, cmd_data_value in cmd_data.items():
            if cmd_data_key == 'number_of_values':
                continue
            if not cmd_data_key in all_averages:
                all_averages[cmd_data_key] = filter_data[cmd_data_key] / filter_data['number_of_values']
                filter_info[cmd_data_key] = []
            cmd_avarage = cmd_data_value / cmd_data['number_of_values']
            if int(100 * cmd_avarage / all_averages[cmd_data_key]) < filtervalue:
                continue
            filter_info[cmd_data_key].append(cmd)
    return filter_info
