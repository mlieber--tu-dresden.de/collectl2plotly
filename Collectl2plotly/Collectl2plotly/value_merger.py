import operator as op_md

# operator lookup table
OPS = {
        '+' : op_md.add,
        '-' : op_md.sub,
        '*' : op_md.mul,
        '/' : op_md.truediv,
        '%' : op_md.mod,
}


def x2oneaddition_int(old_v, *args, roundingpoint=None, operator=None, operator_value=None, **kwargs):
    """sum x values to old_v

    Arguments:
         old_v {int} -- first value

    Keyword Arguments:
        roundingpoint {int} -- int that is given to round function (default: {None})
        operator {str} -- operator that is used to calc if operator_value is given (default: {None})
        operator_value {int} -- value that will be calculate with sum of x values (default: {None})

    Returns:
        int -- sum x values to old_v
    """

    interim_result = sum([int(x) for x in args])

    if operator and operator_value:
        interim_result = OPS[operator](interim_result, operator_value)

    if roundingpoint:
        interim_result = round(interim_result, roundingpoint)
    return old_v + int(interim_result)


def x2oneaddition_float(old_v, *args, roundingpoint=None, operator=None, operator_value=None, **kwargs):
    """sum x values to old_v

    Arguments:
         old_v {float} -- first value

    Keyword Arguments:
        roundingpoint {int} -- int that is given to round function (default: {None})
        operator {str} -- operator that is used to calc if operator_value is given (default: {None})
        operator_value {float/int} -- value that will be calculate with sum of x values (default: {None})

    Returns:
        float -- sum x values to old_v
    """

    interim_result = sum([float(x) for x in args])

    if operator and operator_value:
        interim_result = OPS[operator](interim_result, operator_value)

    if not roundingpoint is None:
        interim_result = round(interim_result, roundingpoint)
    return old_v + interim_result

