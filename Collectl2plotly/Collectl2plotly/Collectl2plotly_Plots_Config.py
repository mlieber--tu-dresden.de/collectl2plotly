### Merge configs
# hint: merger function must be in value_merger.py

# Defaults merge infos
# default_base_value is the startvalue for merging if no is given in NEEDED_VALUES
# default_merger is the merger function that is called if no is given in NEEDED_VALUES
NEEDED_VALUES_DEFAULTS = {
    'default_base_value': 0,
    'default_merger': ('x2oneaddition_int', {}),
}

# Information which values needed for plots
# key  : name of value needed for plotconfig
# value: is dict with following configuration options
#       'keys'   : must be a list with the table heads from collectl that is to be merge
#       'merger' : function that merge the values from the given keys to the new for the plots, parameter of merger
#       'base_value': startvalue for merging
NEEDED_VALUES = {
    'PCT': {'keys':['PCT'], 'merger': ('x2oneaddition_float', {'operator': '/', 'operator_value': 100})},
    'VmRSS': {'keys':['VmRSS'], 'merger': ('x2oneaddition_float', {'operator': '/', 'operator_value': 1048576})},
    'RKBC': {'keys':['RKBC'], 'merger': ('x2oneaddition_float', {'operator': '/', 'operator_value': 1024})},
    'WKBC': {'keys':['WKBC'], 'merger': ('x2oneaddition_float', {'operator': '/', 'operator_value': 1024})},
    'syscalls': {'keys':['RSYS', 'WSYS'], 'merger': ('x2oneaddition_int', {})},
}


### Plots Config

# List of diagrams to be created
# config of Plot
# 'name': Is the name of the Plot will displayed on the website
# 'needed_key': is a key defined in NEEDED_VALUES for the values that are to be used in der Plot as y
# 'plotly_settings': are plotly configs to desing plots see doku for detail
#  hint {host} will be replaced with real hostname in title and in xaxis, yaxis title

barchart_buttons = {
    'annotations': [
        {'text': 'time interval in Barchart:',
         'x': 0.2, 'y': 1.13, 'yref': 'paper', 'xref': 'paper', 'showarrow': False}
    ],
    'updatemenus':[
        {
            'type': 'buttons',
            'x': 0.1,
            'y': 1.15,
            'direction': 'left',
            'buttons': [
                {'args':[{'type': 'scattergl'}, {}],
                'label':'dots',
                'method':'update'},
                {'args':[{'type': 'histogram', 'histfunc': 'avg'}, {'barmode': 'stack'}],
                'label':'barchart',
                'method':'update'},
            ],
            'showactive':True,
        },
        {
            'active': 1,
            'x': 0.4,
            'y': 1.15,
            'showactive': True,
            'buttons' : [
                {
                    'args': ['xbins.size', '1000'],
                    'label': '1 sec',
                    'method': 'restyle',
                },
                {
                    'args': ['xbins.size', '5000'],
                    'label': '5 sec',
                    'method': 'restyle',
                },
                {
                    'args': ['xbins.size', '30000'],
                    'label': '30 sec',
                    'method': 'restyle',
                },
                {
                    'args': ['xbins.size', '60000'],
                    'label': '1 min',
                    'method': 'restyle',
                },
                {
                    'args': ['xbins.size', '300000'],
                    'label': '5 min',
                    'method': 'restyle',
                },
                {
                    'args': ['xbins.size', '1800000'],
                    'label': 'half Hour',
                    'method': 'restyle',
                },
                {
                    'args': ['xbins.size', '3600000'],
                    'label': 'Hour',
                    'method': 'restyle',
                },
                {
                    'args': ['xbins.size', '43200000'],
                    'label': 'half Day',
                    'method': 'restyle',
                },
            ]
        }
    ],
}

PLOTS_CONFIG = [{
        'name': 'CPU load',
        'needed_key': 'PCT',
        'relative-absolute_xaxis': True,
        'plotly_settings': {
            'data': {
                'type': 'scattergl',
                'mode': 'markers',
            },
            'layout': {
                'height': 500,
                'title': {'text': 'CPU load {host}', 'y': 1},
                'xaxis': {'title': 'Date'},
                'yaxis': {'title': 'CPU load'},
                'showlegend': True,
                **barchart_buttons
            },
            'config': {
                'toImageButtonOptions': {
                    'format': 'png',
                    'filename': 'cpu_load_plot',
                    'height': 900,
                    'width': 1600,
                    'scale': 1,
                },
            },
        },
    },
    {
        'name': 'RAM usage',
        'needed_key': 'VmRSS',
        'relative-absolute_xaxis': True,
        'plotly_settings': {
            'data': {
                'type': 'scattergl',
                'mode': 'markers',
            },
            'layout': {
                'height': 500,
                'title': {'text': 'Memory Usage {host}', 'y': 1},
                'xaxis': {'title': 'Date'},
                'yaxis': {'title': 'RAM usage GiB'},
                'showlegend': True,
                **barchart_buttons
            },
            'config': {
                'toImageButtonOptions': {
                    'format': 'png',
                    'filename': 'ram_usage_plot',
                    'height': 900,
                    'width': 1600,
                    'scale': 1,
                },
            },
        },
    },
    {
        'name': 'I/O read',
        'needed_key': 'RKBC',
        'relative-absolute_xaxis': True,
        'plotly_settings': {
            'data': {
                'type': 'scattergl',
                'mode': 'markers',
            },
            'layout': {
                'height': 500,
                'title': {'text': 'I/O read {host}', 'y': 1},
                'xaxis': {'title': 'Date'},
                'yaxis': {'title': 'I/O MiB/s', 'type': 'log'},
                'showlegend': True,
                **barchart_buttons
            },
            'config': {
                'toImageButtonOptions': {
                    'format': 'png',
                    'filename': 'io_read_plot',
                    'height': 900,
                    'width': 1600,
                    'scale': 1,
                },
            },
        },
    },
    {
        'name': 'I/O write',
        'needed_key': 'WKBC',
        'relative-absolute_xaxis': True,
        'plotly_settings': {
            'data': {
                'type': 'scattergl',
                'mode': 'markers',
            },
            'layout': {
                'height': 500,
                'title': {'text': 'I/O write {host}', 'y': 1},
                'xaxis': {'title': 'Date'},
                'yaxis': {'title': 'I/O MiB/s', 'type': 'log'},
                'showlegend': True,
                **barchart_buttons
            },
            'config': {
                'toImageButtonOptions': {
                    'format': 'png',
                    'filename': 'io_write_plot',
                    'height': 900,
                    'width': 1600,
                    'scale': 1,
                },
            },
        },
    },
    {
        'name': 'I/O syscalls',
        'needed_key': 'syscalls',
        'relative-absolute_xaxis': True,
        'plotly_settings': {
            'data': {
                'type': 'scattergl',
                'mode': 'markers',
            },
            'layout': {
                'height': 500,
                'title': {'text': 'I/O syscalls {host}', 'y': 1},
                'xaxis': {'title': 'Date'},
                'yaxis': {'title': 'I/O syscalls/s', 'type': 'log'},
                'showlegend': True,
                **barchart_buttons
            },
            'config': {
                'toImageButtonOptions': {
                    'format': 'png',
                    'filename': 'io_syscalls_plot',
                    'height': 900,
                    'width': 1600,
                    'scale': 1,
                },
            },
        },
    },
]


### Name parse configs

# dict of all commands where the name will be filtered out
# key  : command
# value: list of parameter that take a value what is not the name
NAME_SPEZIAL_PARAMETER = {
    'java': ['-cp', '-classpath'],
    'bash': [],
    'sh': [],
    'perl': [],
    'python': ['-m', '-W', '-X', '--check-hash-based-pycs', '-c'],
}

### other Configs

# list of commands that are ignored
COMAND_BLACKLIST = [
    'collectl',
]

# list of colors of the plot entrys
PLOTLY_COLORS=['rgb(31, 119, 180)', 'rgb(255, 127, 14)',
                'rgb(44, 160, 44)', 'rgb(214, 39, 40)',
                'rgb(148, 103, 189)', 'rgb(140, 86, 75)',
                'rgb(227, 119, 194)', 'rgb(127, 127, 127)',
                'rgb(188, 189, 34)', 'rgb(23, 190, 207)']

PLOTLY_STATIC_COLORS = {}