## Validation of read write and syscall counts of collectl
The validate_read_write_syscalls.cpp is a programm what do following things once per second:
1. writes exactly one kib
2. read exactly one kib
3. And that are exactly 3 read/write syscalls in C++ because:
    1 write to file
    2 read because the file must be reopened in every loop otherwise it will only do a read command once

And collectl save this correctly!

## Validation of CPU
Test was install "stress" and stress one cpu and the cpu load of stress must be one cpu:
```
    stress -c 1
```

In my test the collectl has collect all data right.

## Validation of Memory

Programm validate_ram.cpp Allocate 1GB memory(for double) but C++ need for this more memory so it is not a good way do validate the data of collectl. 
But the memory that is shown in htop is the same like in collectl so I assume the values are correct.