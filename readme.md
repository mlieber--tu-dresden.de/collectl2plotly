# Collectl2plotly

Collectl2plotly is a python commandline tool, to build htmlsites with plotly diagrams from data that was collected with [collectl](http://collectl.sourceforge.net).

Collectl wiki is [here](http://collectl.sourceforge.net).

## Example Plot
This is how a plot can looks like.
![plot](example_plot.png)


## How to
To use you need to install it via pip and collectl must be installed:
```
    pip install ./Collectl2plotly
```

After that you can use it like that:
```
    Collectl2plotly <options>
```
For description of the options use "--help" or go to the [options](#options) description.

## Collectl command to create input files for Collectl2plotly
Collectl2plotly plots process data from collectl.
To record process data, use the collectl option "-sZ".
```
    collectl -f <filename that will be created> -i :<sample interval in seconds> -sZ
```
You can also use collectl data from multiple cluster nodes that was recorded at the same time.

## Options

| Option          | Takes | Description  | Default |
| --------------- | ----- | ------------ | ------- |
| -s, --source | a path to a directory with collectl datafiles(.raw.gz) or collectl datafile directly. | It is multiple useable! It will be used as collectl sources to get the Plotdata. | If no source is given it searches in the current dir |
| -c, --collectl  | how to call collectl | is the collectl that will be called to get (replay) the data from the sources | collectl without a path |
| -p, --plotlypath | a path to a plotly javascript libary | is needed for the plot in the html files | plotly.js next to skript |
| -d, --destination | a path to a directory | is where a directory with the html files will be created | current directory |
| --configpath | a path to config file | is a python file with the plot and merge settings see [here](#config)| a default config |
| --shorten /<br> --noshorten | - | enable or disable shorten of commands with parameters/options only to file/command names. <br> examples:<br> python ~/scripts/script.py 1 --> script.py <br>ls -lisa --> ls | enabled |
| --coarsest | - | enable shorten of commands only to command names.<br> If enabled --shorten is ignored!<br> examples: <br> python ~/scripts/script.py 1 --> python <br> ls -lisa --> ls | disabled |
| --filtercmds / --nofiltercmds | - | enable or disable [filtering](#filter) | enabled |
| --filtertype | filtertype <br> see --help for detail | to select which filter to be used | see --help |
| --filtervalue | any value (string, int, ...) | is passed to the filter function as string | - |
| --showcommands | - | print all commands of the plots to copy for static colors | - |
| -f, --force | - | override existing plot directory if exist | - |


## Config
The config is a python file with the following varibles to create special plots or/and with different values instead of the default.
Usually, the default should be ok and there is no need to write a custom config.

In the config you define how the values are merged.

Merging means that all values from "collectl" will be accumulated (using a merging function) to already existing values of the command/time point or a base value, if no values exist for that command/time point. By accumulating the values, they are also converted and formatted.

Note:

Filtering is not set in the config you filter with the [options](#options) of the command.
With filtering is mean sort out commands that are not relevant enough to be displayed in the diagrams to keep the diagramm clean.

#### NEEDED_VALUES
Specifies which values are merged under which names in a dict.
As key is the name of the merged values and the dict value is config with:

**'keys'**:<br>
This are a list of collectl table headers of the values which are given into the merger function.

**'merger'**:<br>
Is a tuple with the merger function name and a dict with the parameter of the merger. See [merger](#merger) to know what merger and parameter exist.

**'base_value'**:<br>
Is the startvalue of merger.

If 'merger' or 'base_value' is not given it use the default from [NEEDED_VALUES_DEFAULTS](#needed_values_defaults).

Example:
```python
NEEDED_VALUES = {
    'CPU': {
        'keys':['PCT'],
        'merger': ('x2oneaddition_float', {'operator': '/', 'operator_value': 100}),
        'base_value': 0.0,
    },
    'syscalls': {
        'keys':['RSYS', 'WSYS'],
        'merger': ('x2oneaddition_int', {}),
    },
}
```


#### NEEDED_VALUES_DEFAULTS
Defines the defaults of NEEDED_VALUES.

Example:
```python
NEEDED_VALUES_DEFAULTS = {
    'default_base_value': 0,
    'default_merger': ('x2oneaddition_int', {}),
}
```

#### NAME_SPEZIAL_PARAMETER_CONFIG
Specifies for which commands a script/program name is searched (dict key)and
which parameters the command has, that takes another value that is not the name (dict value). This is required to identify the name.

Important:<br>
Versions and path are ignored on the command example: python3 and /bin/python2 are both only python.
Bash -c and sh -c are an exception and are treated separately in the code.

Example:
```python
NAME_SPEZIAL_PARAMETER_CONFIG = {
    'java': ['-cp', '-classpath'],
    'bash': [],
    'sh': [],
    'perl': [],
    'python': ['-m', '-W', '-X', '--check-hash-based-pycs', '-c'],
}
```

#### COMAND_BLACKLIST
Is a list of commands which are ignored. It use the shorted command to check it is in blacklist or not.

Example:
```python
COMAND_BLACKLIST = [
    'collectl',
]
```

#### PLOTS_CONFIG
Is a list of plots settings of the plots to be displayed on the pages.
```
PLOTS_CONFIG = [
    {Plot1},
    {Plot2},
    ...
]
```

A plot settings is dict with following keys:

**'name'**:<br>
Is the name of the Plot this will be displayed in the index of the websites.

**'needed_key'**:<br>
Is a name from NEEDED_VALUES this values are used as y axis data in the plot.

**'relative-absolute_xaxis'**:<br>
Is a bool if True add buttons to the plot that switch the xaxis from absolute Date to Runtime and back.
If not given it is False.

**'plotly_settings'**:<br>
Is a settings dict in Plotly style, here it is possible to set everything about the appearance of the plots.<br>
Hints for plotly_settings:
+ Use [Plotly wiki](https://plot.ly/javascript/) javascript  to search settings because the javascript objekt looks same like the python dict.
+ If you use same settings in multiple plots you can put in a dict variable first and included on the right possition like "**variable".
+ {host} will replaced with the hostname in title of plot and in y/xaxis.

Example:
```python
PLOTS_CONFIG = [
    {
        'name': 'CPU load',
        'needed_key': 'CPU',
        'relative-absolute_xaxis': True,
        'plotly_settings': {
            'data': {
                'type': 'scattergl',
                'mode': 'markers',
            },
            'layout': {
                'height': 500,
                'title': {'text': 'CPU load {host}', 'y': 1},
                'xaxis': {'title': 'Date'},
                'yaxis': {'title': 'CPU load'},
                'showlegend': True,
                **barchart_buttons
            },
        },
    },
]
```

#### PLOTLY_COLORS
Is a list of colors to be used for the plots.
Repeats itself when there are not enough colors for the plot.

Example:
```python
PLOTLY_COLORS=[
    'rgb(31, 119, 180)', 'rgb(255, 127, 14)',
    'rgb(44, 160, 44)', 'rgb(214, 39, 40)',
    'rgb(148, 103, 189)', 'rgb(140, 86, 75)',
    'rgb(227, 119, 194)', 'rgb(127, 127, 127)',
    'rgb(188, 189, 34)', 'rgb(23, 190, 207)',
]
```

#### PLOTLY_STATIC_COLORS
Is to give one comand a specific Color.

Key is the displayed name in plot.<br>
Value is color as string.

Example:
```python
PLOTLY_STATIC_COLORS = {
    'example.py': 'rgb(254, 1, 154)',
}

# if not needed do:
PLOTLY_STATIC_COLORS = {}
```

## Filter
Filters are used to sort out commands that are not relevant enough to be displayed in the diagrams.

### Existing filter
| Name | Name of function | Description |  filtervalue default |
| ---- | ---------------- | ----------- |  ------------------- |
| hardvalue | filter_hardvalue | Filter out all that are above 'filtervalue' when adding from largest to smallest and not zero. | 95(%) |
| average | filter_average | Filters out those commands that are on average below the 'filtervalue' relative to the overall average. <br>Default: filtervalue=5(percent) | 5(%) |

#### Filter example
hardvalue:
```
    filtervalue = 95
 
    values: for each command the sum of all recorded values: cmd1, cmd2, ... cmd6
    values = [0, 1, 4, 4, 5, 6]

    calculate percentages from values relative to sum of all values
    example on 5:
    25 =  5 * 100 / 20 (sum of all values)

    new_values are [0, 5, 20, 20, 25, 30]
    sort from greates to smallest
    new_values = [30, 25, 20, 20, 5, 0]

    go over new_values and sum up until sum is greater than or equal to filtervalue, the remaining values are filtered out
    30 + 25 + 20 + 20 == 95
    => [6, 5, 4, 4] == [cmd6, cmd5, cmd4, cmd3]
```
average:
```
    filtervalue = 5
    values: for each command the average of all recorded values: cmd1, cmd2, ... cmd6
    values = [0, 1, 4, 4, 5, 6]

    calculate percentages from values relative to average of all values
    example on 5:
    150 =  5 * 100 / 3.33 (average of all values)

    if percentage is less than filtervalue the cmd is filterd out

    end result [1, 4, 4, 5, 6] == [cmd2, cmd3, cmd4, cmd5, cmd6]
```

### Add new filter
Two filter functions are pre-defined. Additional filters can be added in filter_func.py.
If you add one filter extend 'FILTER_FUNCTIONS' in Collectl2plotly.py with your filter.

#### Arguments of a filter function
1. Is a dict that looks like:
```python
{
    'number_of_values': X all,
    'commands':{
        cmd:{'number_of_values': X command, metric:SUM of command, ...}
    },
    metrics:SUM of all commands, ...
}
```
2. Is a string from the option filtervalue or None if no filtervalue given as option on execution.

#### Return
A filter function must return a dict, structured like this:
```
    {
        metric: [command that is not filter out, ...],
        ... for all metrics
    }
```

## Merger
A merger is a function that merge values from collectl to a existing value or a base value what is set in the config see [here](NEEDED_VALUES). 

It is also used to format and convert the values from collectl.

### Existing merger

| Name | Description | return type |
| ---- | ----------- | ----------- |
| x2oneaddition_int | convert collectl value to int and sum all values | int |
| x2oneaddition_float | convert collectl value to float and sum all values | float |


 | possible argument | take | describtion | supported merger |
 | ----------------- | ---- | ----------- | ---------------- |
 | round | int | Rounding merged collectl values on given point (Python round function). | x2oneaddition_* |
 | operator | basic python operater as str (/ , * , -, +, %) | Specifies which operator should be used to calculate a value from parameter operator_value and the collectl values. This will be merged to the base value. | x2oneaddition_* |
 | operator_value | int/float | value that must be given to use operator for use see operator. | x2oneaddition_* |

### Add new Merger
Two mergers are pre-defined. Additional ones can be added in value_merger.py.
If one merger is added in value_merger.py it must be added in the global variable MERGER in Collectl2plotly.py.

#### Arguments of a merger function
1. base value is the initial value for merging on it. Normally it has the same type like the return value
because the return value can possible the next base value.
2. (args) All following arguments are the values(str) from the collectl that were assigned in [NEEDED_VALUES](#needed_values).
3. (kwargs) Are the Parameter that were assigned in [NEEDED_VALUES](#needed_values) for the merger.

#### Return
A merger function must return a value that can be the base value of this merger
(mergers can be called multiple times for the same time stamp if multiple samples exist).

